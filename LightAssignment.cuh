/*********************************************************** -- HEAD -{{{1- */
/** Light Assignment
 */
/******************************************************************* -}}}1- */

#ifndef LIGHTASSIGNMENT_CUH_45BA0003_370F_48F9_A1F1_1411C0FDE16F
#define LIGHTASSIGNMENT_CUH_45BA0003_370F_48F9_A1F1_1411C0FDE16F

//--//////////////////////////////////////////////////////////////////////////
//--    Include                     ///{{{1///////////////////////////////////

#include <cassert>

#include <linmath/Aabb.h>
#include <utils/IntTypes.h>

#include <chag/pp/ext/unit.cuh>
#include <chag/pp/ext/range.cuh>
#include <chag/pp/aspect/operators.cuh>


//--    Hierarchical                ///{{{1///////////////////////////////////
void light_hierarchy_compute_light_aabb( 
	const float4* aLightsStart, 
	const float4* aLightsEnd,
	float4* aLightAABBMin,
	float4* aLightAABBMax
);

template< class tSortKey, class tLightData >
__global__ void light_hierarchy_make_keys(
	const tSortKey aSortKey,
	const tLightData aLightData,
	typename tSortKey::KeyType* aSortKeys,
	uint32_t* aLightIndices
);

template< class tLightData, class tHierarchy >
__global__ void light_hierarchy_build_from_leaves(
	const tLightData aLightData,
	const tHierarchy aHierarchy,
	const uint32_t* aLightIndexTable,
	const float4* aLightAABBMin, // HACK: also setup root node here
	const float4* aLightAABBMax
);
template< class tHierarchy >
__global__ void light_hierarchy_build_upper(
	const tHierarchy aHierarchy,
	unsigned aFromLevel
);

template< class ClusterAabbBuilder, class tLightData, class tHierarchy >
__global__ void light_hierarchy_assign_count( 
	const tLightData aLightData,
	const tHierarchy aHierarchy,
	const typename ClusterAabbBuilder clusterAabbBuilder, const uint32_t* __restrict__ clusterKeys,
	unsigned aNumClusters,
	uint32_t* aClusterLightCounts
);

__global__ void light_hierarchy_assign_scan(
	const uint32_t* aClusterLightCounts,
	unsigned aNumClusters,
	unsigned aBufferSize, // must be multiple of 1024!
	uint32_t* aClusterLightOffsets
);

template< class ClusterAabbBuilder, class tLightData, class tHierarchy >
__global__ void light_hierarchy_assign_store(
	const tLightData aLightData,
	const tHierarchy aHierarchy,
	const typename ClusterAabbBuilder clusterAabbBuilder, const uint32_t* __restrict__ clusterKeys,
	const uint32_t* aClusterLightOffsets,
	unsigned aNumClusters,
	uint32_t* aClusterLightIndices
);

//--    Hierarchy                   ///{{{1///////////////////////////////////
class LightHierarchy32
{
	public:
		inline LightHierarchy32();
		//inline ~LightHierarchy32(); // invoked as part of the kernel invoc

	public:
		inline __host__ void set_light_count( unsigned aNumLights );
		inline __host__ void get_node_aabbs( 
			float4* aNodeMins, 
			float4* aNodeMaxs 
		) const;

	public:
		inline __device__ __host__ unsigned get_num_nodes() const;
		inline __device__ __host__ unsigned get_num_levels() const;
		inline __device__ __host__ unsigned get_level_start( unsigned aLevel ) const;
		inline __device__ __host__ unsigned get_level_nodes( unsigned aLevel ) const;

	public:
		inline __device__ void set_root(
			const float4* aRootMin,
			const float4* aRootMax
		) const;
		inline __device__ void set_node(
			unsigned aLevel,
			unsigned aNodeId,
			const float3& aNodeMin,
			const float3& aNodeMax 
		) const;
		inline __device__ void set_leaf(
			unsigned aLeafId,
			unsigned aLightIndex
		) const;

		inline __device__ float3 get_node_min( unsigned aIndex ) const;
		inline __device__ float3 get_node_max( unsigned aIndex ) const;

		template< class tLightData>
		inline __device__ unsigned count_light_overlaps(const tLightData& aLightData, const float3 aabbMin, const float3 aabbMax) const;

		template< class tLightData>
		inline __device__ unsigned store_light_overlaps(const tLightData& aLightData, const float3 aabbMin, const float3 aabbMax, 
			unsigned aClusterOffset, uint32_t* aClusterLightIndices) const;

	private:
		template< unsigned tLevel, unsigned tMaxLevel, class tLightData >
		inline __device__ unsigned count_lo_upper_(
			const tLightData& aLightData,
			const float3 aabbMin, const float3 aabbMax,
			unsigned aIndex
		) const;
		template< unsigned tLevel, class tLightData >
		inline __device__ unsigned count_lo_leaves_(
			const tLightData& aLightData,
			const float3 aabbMin, const float3 aabbMax,
			unsigned aIndex 
		) const;

		template< unsigned tLevel, unsigned tMaxLevel, class tLightData >
		inline __device__ unsigned store_lo_upper_(
			const tLightData& aLightData,
			const float3 aabbMin, const float3 aabbMax,
			unsigned aIndex, 
			unsigned aClusterOffset,
			uint32_t* aClusterLightIndices
		) const;
		template< unsigned tLevel, class tLightData >
		inline __device__ unsigned store_lo_leaves_(
			const tLightData& aLightData,
			const float3 aabbMin, const float3 aabbMax,
			unsigned aIndex,
			unsigned aClusterOffset,
			uint32_t* aClusterLightIndices
		) const;

		template< unsigned tLevel, unsigned tMaxLevel >
		friend struct LhInvokeCount;
		template< unsigned tLevel, unsigned tMaxLevel >
		friend struct LhInvokeStore;

	private:
		unsigned m_numLights;
		unsigned m_numLevels;
		unsigned m_numNodes;

		unsigned m_numMaxNodes;
		unsigned m_numMaxLights;

		float4* m_nodesMin;
		float4* m_nodesMax;
		uint32_t* m_lightIndices;
};

//--///}}}1////////////// vim:syntax=cuda:foldmethod=marker:ts=4:noexpandtab: 
#include "LightAssignment.cu.inl"
#endif // LIGHTASSIGNMENT_CUH_45BA0003_370F_48F9_A1F1_1411C0FDE16F
