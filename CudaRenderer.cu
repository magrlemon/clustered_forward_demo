#include "CudaRenderer.h"
#include "utils/CudaHelpers.h"
#include <chag/pp/prefix.cuh>
#include "LightAssignment.cuh"
#include <thrust/sort.h>
#include <thrust/device_ptr.h>
#include "utils/CudaResource/CudaTexture.h"
#include "utils/CudaResource/CudaBuffer.h"
#include "utils/CudaBuffer.h"
#include "utils/CudaCheckError.h"
#include <cuda_gl_interop.h>
#include <performance_monitor/profiler/Profiler.h>

#undef near
#undef far


class CudaRendererImpl : public CudaRenderer
{
public:

	virtual void init(GlBufferObject<uint32_t> &fullClusterBuffer, GlBufferObject<chag::uint2> &fullClusterGridBuffer, 
		GlBufferObject<chag::float4> &lightPositionRangeBuffer, GlBufferObject<chag::float4> &lightColorBuffer, GlBufferObject<int> &tileLightIndexListsBuffer);

	/**
	 * Assumes lights are in buffers already...
	 */
	virtual void buildClusters(const Globals &globals, uint32_t numLights);

protected:
	void assignLights(uint32_t numLights, uint32_t numClusters, const chag::float4 *lightPositionRangeCuda);

	Globals m_globals;

	// Resources that are mapped from OpenGL.
	//// cluster flag buffer
	chag::CudaBufferObject<uint32_t> *m_clusterFlagBuffer;
	chag::CudaBufferObject<chag::uint2> *m_clusterResultBuffer;

	chag::CudaBufferObject<const chag::float4> *m_lightPositionRangeBuffer;
	chag::CudaBufferObject<const chag::float4> *m_lightColorBuffer;
	chag::CudaBufferObject<uint32_t> *m_tileLightIndexListBuffer;
	CudaBuffer<uint32_t> m_tmpBuffer;
	CudaBuffer<uint32_t> m_numClustersCuda;

	CudaBuffer<uint32_t> m_uniqueClusterIds;
	CudaBuffer<uint32_t> m_uniqueClusterIndexes;
  CudaBuffer<uint32_t> m_clusterLightCounts;
  CudaBuffer<uint32_t> m_clusterLightDataOffsets;

	CudaBuffer<uint32_t> m_lightKeys;
	CudaBuffer<uint32_t> m_lightIndices;
	CudaBuffer<chag::float4> m_lightAABBMin;
	CudaBuffer<chag::float4> m_lightAABBMax;


	LightHierarchy32 *m_lightHierarchy;
};



CudaRenderer *CudaRenderer::create()
{
	int deviceCount = 0;
	if (cudaGetDeviceCount(&deviceCount) == cudaSuccess)
	{
		printf("CUDA detected, initializing\n");
		int cudaDevice = 0; // TODO -- which device should we use?
		cutilSafeCall(cudaGLSetGLDevice(cudaDevice));
		CUT_CHECK_ERROR("cudaGLSetGLDevice");

		return new CudaRendererImpl;
	}
	else
	{
		printf("NO CUDA device/driver detected, CPU fallback will be used.\n");
		Profiler::setEnabledTimers(Profiler::TT_Cpu | Profiler::TT_OpenGl);
	}

	return 0;
}


void CudaRendererImpl::init(GlBufferObject<uint32_t> &fullClusterBuffer, GlBufferObject<chag::uint2> &fullClusterGridBuffer, 
	  GlBufferObject<chag::float4> &lightPositionRangeBuffer, GlBufferObject<chag::float4> &lightColorBuffer, GlBufferObject<int> &tileLightIndexListsBuffer)
{
	m_tileLightIndexListBuffer = new chag::CudaBufferObject<uint32_t>(tileLightIndexListsBuffer, cudaGraphicsRegisterFlagsNone);
	m_clusterResultBuffer = new chag::CudaBufferObject<chag::uint2>(fullClusterGridBuffer, cudaGraphicsRegisterFlagsWriteDiscard);
	m_lightPositionRangeBuffer = new chag::CudaBufferObject<const chag::float4>(lightPositionRangeBuffer, cudaGraphicsRegisterFlagsReadOnly);
	m_lightColorBuffer = new chag::CudaBufferObject<const chag::float4>(lightColorBuffer, cudaGraphicsRegisterFlagsReadOnly);
	m_clusterFlagBuffer = new chag::CudaBufferObject<uint32_t>(fullClusterBuffer, cudaGraphicsRegisterFlagsNone);

	m_tmpBuffer.init(LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y * LIGHT_GRID_MAX_DIM_Z);
	m_numClustersCuda.init(1);
	// allocate some, grows as needed.
	m_uniqueClusterIds.init(LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y);
	m_uniqueClusterIndexes.init(LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y);
	m_clusterLightCounts.init(LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y);
	m_clusterLightDataOffsets.init(LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y);

	m_lightKeys.init(NUM_POSSIBLE_LIGHTS);
	m_lightIndices.init(NUM_POSSIBLE_LIGHTS);
	m_lightAABBMin.init(1);
	m_lightAABBMax.init(1);

	m_lightHierarchy = new LightHierarchy32;
}



struct ClusterKey
{
	static const uint32_t g_invalidIdPacked = 0xFFFFFFFF;

	static const uint32_t xBits = 8;
	static const uint32_t yBits = 8;
	static const uint32_t zBits = 10;

	static const uint32_t xShift = yBits + zBits;
	static const uint32_t yShift = zBits;
	static const uint32_t zShift = 0;

	static const uint32_t xMask = (1UL << xBits) - 1UL;
	static const uint32_t yMask = (1UL << yBits) - 1UL;
	static const uint32_t zMask = (1UL << zBits) - 1UL;


  uint32_t x;
  uint32_t y;
  uint32_t z;
  uint32_t packedNormal;

  bool operator < (const ClusterKey &o) const
  {
    return memcmp(this, &o, sizeof(*this)) < 0;
  }

	static ClusterKey s_invalid;

	static CUDA_HOST_DEVICE_PREAMBLE ClusterKey unpack(uint32_t packed)
	{
		ClusterKey key;

		// TODO: support the other dimesions!
		key.packedNormal = 0;

		key.x = (packed >> xShift) & xMask;
		key.y = (packed >> yShift) & yMask;
		key.z = (packed >> zShift) & zMask;

		return key;
	}
	static CUDA_HOST_DEVICE_PREAMBLE uint32_t pack(ClusterKey key)
	{
		// TODO: support the other dimesions!
		// TODO: Add asserts to check for bit overflow. 
		return ((key.x & xMask) << xShift) 
		     | ((key.y & yMask) << yShift) 
		     | ((key.z & zMask) << zShift);
	}

	//static CUDA_HOST_DEVICE_PREAMBLE ClusterKey unpackMorton(uint32_t packed)
	//{
	//	ClusterKey key;

	//	// TODO: support the other dimesions!
	//	key.packedNormal = 0;

	//	key.x = unspreadBits<xBits>(packed, 3, 2);
	//	key.y = unspreadBits<yBits>(packed, 3, 1);
	//	key.z = unspreadBits<zBits>(packed, 3, 0);

	//	return key;
	//}
	//static CUDA_HOST_DEVICE_PREAMBLE uint32_t packMorton(ClusterKey key)
	//{
	//	// TODO: support the other dimesions!
	//	// TODO: Add asserts to check for bit overflow. 
	//	return spreadBits_3_10bit(key.x, 2)
	//	     | spreadBits_3_10bit(key.y, 1)
	//	     | spreadBits_3_10bit(key.z, 0);

	//}
};



__global__ void storeUniqueClusters(uint32_t num, const uint32_t *__restrict__ clusterFlags, const uint32_t *__restrict__ uniqueOffset, uint32_t *__restrict__ uniqueClustersIds, uint32_t *__restrict__ uniqueClustersIndexes)
{
	uint32_t index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index < num)
	{
		if (clusterFlags[index])
		{
			// decode coordinate
			ClusterKey key;
			key.x = index % LIGHT_GRID_MAX_DIM_X;
			key.y = (index / LIGHT_GRID_MAX_DIM_X) % LIGHT_GRID_MAX_DIM_Y;
			key.z = index / (LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y);

			uniqueClustersIds[uniqueOffset[index]] = ClusterKey::pack(key);
			uniqueClustersIndexes[uniqueOffset[index]] = index;
		}
	}
}

texture<float4, 1, cudaReadModeElementType> g_light_position_range_tex;
texture<float4, 1, cudaReadModeElementType> g_light_color_tex;

__constant__ uint32_t g_numLights;

struct LightData
{
	__device__ inline uint32_t light_count() const { return g_numLights; } 
	__device__ inline float4 get_light_range( uint32_t aIdx ) const
	{
		return tex1Dfetch( g_light_position_range_tex, aIdx );
	}
	__device__ inline float4 get_light_color( uint32_t aIdx ) const
	{
		assert(false);
		return tex1Dfetch( g_light_color_tex, aIdx );
	}
};


class LightDataBuffer
{
public:
	LightDataBuffer( uint32_t aNumLights, float4* aLightData )
	{
		m_numLights = aNumLights;
		m_lightData = aLightData;
	}

public:
	__device__ inline unsigned light_count() const { return m_numLights; }
	__device__ inline float4 get_light_range( unsigned i ) const {
		assert( i < m_numLights );
		return m_lightData[i];
	}

private:
	uint32_t m_numLights;
	float4* m_lightData;
};



//--    Compatibility               ///{{{1///////////////////////////////////
struct ClusterAabbBuilder
{
	inline ClusterAabbBuilder( const Globals& aConsts, float aSD );
		
	__device__ inline void getAabb( uint32_t packedClusterKey, ::float3 &aabbMin, ::float3 &aabbMax) const;

	float ratioX, ratioY;
	float recGridDimX, recGridDimY;
	float sD1; // sD + 1.0f
};



inline ClusterAabbBuilder::ClusterAabbBuilder( const Globals& aConsts, float aSD )
{
	ratioX = tanf( toRadians(aConsts.fov) * 0.5f ) * aConsts.aspectRatio;
	ratioY = tanf( toRadians(aConsts.fov) * 0.5f );

	//recGridDimX = 1.0f / ((aConsts.fbWidth+LIGHT_GRID_TILE_DIM_X-1)/LIGHT_GRID_TILE_DIM_X);
	//recGridDimY = 1.0f / ((aConsts.fbHeight+LIGHT_GRID_TILE_DIM_Y-1)/LIGHT_GRID_TILE_DIM_Y);

	recGridDimX = float(LIGHT_GRID_TILE_DIM_X) / aConsts.fbWidthf;
	recGridDimY = float(LIGHT_GRID_TILE_DIM_Y) / aConsts.fbHeightf;

	sD1 = aSD + 1.0f;
}


__device__ inline void ClusterAabbBuilder::getAabb( uint32_t packedClusterKey, ::float3 &aabbMin, ::float3 &aabbMax) const
{																																															 
	ClusterKey ck = ClusterKey::unpack(packedClusterKey);

	float viewTileNear = g_globals.near * powf(sD1, float(ck.z));
	float viewTileFar = viewTileNear * sD1;

	// make view space aabb from key...
	float xQ = ratioX * (-1.0f + 2.0f * recGridDimX * float(ck.x));
	float xQ2 = ratioX * (-1.0f + 2.0f * recGridDimX * float(ck.x + 1));
	float yQ = ratioY * (-1.0f + 2.0f * recGridDimY * float(ck.y));
	float yQ2 = ratioY * (-1.0f + 2.0f * recGridDimY * float(ck.y + 1));

	aabbMin = make_float3( min(viewTileNear * xQ, viewTileFar * xQ),   min(viewTileNear * yQ, viewTileFar * yQ), -viewTileFar );
	aabbMax = make_float3( max(viewTileNear * xQ2, viewTileFar * xQ2), max(viewTileNear * yQ2, viewTileFar * yQ2), -viewTileNear );
}



void CudaRendererImpl::assignLights(uint32_t numLights, uint32_t numClusters, const chag::float4 *lightPositionRangeCuda)
{
	PROFILE_SCOPE_2("assignLights", TT_Cuda);

	// If no lights are visible, just clear the offset and count buffers.
	if (numLights == 0)
	{
		m_clusterLightCounts.resize( numClusters, true );
		megaMemset(m_clusterLightCounts.cudaPtr(), 0, m_clusterLightCounts.size());
		m_clusterLightDataOffsets.resize( numClusters, true );
		megaMemset(m_clusterLightDataOffsets.cudaPtr(), 0, m_clusterLightDataOffsets.size());
	}
	else
	{
		// Light data
		LightDataBuffer lightData(numLights, (::float4*)lightPositionRangeCuda);

		// initialize light data
		cudaBindTexture(0, g_light_position_range_tex, lightPositionRangeCuda, numLights * sizeof(lightPositionRangeCuda[0]));
		LightData lightDataTexture;

		// -- build light hierarchy --
		{
			PROFILE_SCOPE_2("buildLightHierarchy", TT_Cuda);
			// initialize buffers
			m_lightKeys.resize( numLights, true );
			m_lightIndices.resize( numLights, true );

			m_lightAABBMin.resize( 1, true );
			m_lightAABBMax.resize( 1, true );

			// update LightHierarchy setup
			m_lightHierarchy->set_light_count( numLights );

			// kernel params
			dim3 threadsA( 6 * 32, 1, 1) ;
			dim3 blocksA( (numLights+threadsA.x-1)/threadsA.x, 1, 1 );
			{
				PROFILE_SCOPE_2("light_hierarchy_compute_light_aabb", TT_Cuda);

				// generate light AABBs
				light_hierarchy_compute_light_aabb( (const float4*)lightPositionRangeCuda, (const float4*)lightPositionRangeCuda + numLights, (float4*)m_lightAABBMin.cudaPtr(), (float4*)m_lightAABBMax.cudaPtr() );
			}

			{
				PROFILE_SCOPE_2("light_hierarchy_make_keys", TT_Cuda);
				light_hierarchy_make_keys<<<blocksA, threadsA>>>( (const float4*)m_lightAABBMin.cudaPtr(), (const float4*)m_lightAABBMax.cudaPtr(), lightData, m_lightKeys.cudaPtr(), m_lightIndices.cudaPtr() );
			}

			{
				PROFILE_SCOPE_2("thrust::stable_sort_by_key", TT_Cuda);
				// sort
				thrust::device_ptr<uint32_t> thrustKeys( m_lightKeys.cudaPtr() );
				thrust::device_ptr<uint32_t> thrustIndices( m_lightIndices.cudaPtr() );
				thrust::stable_sort_by_key( thrustKeys, thrustKeys + numLights, thrustIndices );
			}
			CHECK_CUDA_ERROR();

			// cta layout & co
			unsigned numLeafWarps = (numLights + 32 - 1) / 32;

			dim3 threadsB = dim3( 32, 6, 1 );
			dim3 blocksB = dim3( (numLeafWarps+6-1)/6, 1, 1 );

			{
				PROFILE_SCOPE_2("light_hierarchy_build_from_leaves", TT_Cuda);
				// build leaf nodes
				light_hierarchy_build_from_leaves<<<blocksB, threadsB >>>( lightData, *m_lightHierarchy, m_lightIndices.cudaPtr(), (const float4*)m_lightAABBMin.cudaPtr(), (const float4*)m_lightAABBMax.cudaPtr() );
			}
			{
				PROFILE_SCOPE_2("light_hierarchy_build_upper", TT_Cuda);
				// build rest of tree
				for( int i = m_lightHierarchy->get_num_levels()-3; i > 0; --i )
				{
					unsigned nodeCount = m_lightHierarchy->get_level_nodes(i);

					dim3 t = dim3( 32, 6, 1 );
					dim3 b = dim3( (nodeCount+6-1)/6, 1, 1 );

					light_hierarchy_build_upper<<<b,t>>>( *m_lightHierarchy, i+1 );
				}
			}
			CHECK_CUDA_ERROR();
		}

		// find light overlaps
		{
			PROFILE_SCOPE_2( "find_light_overlaps", TT_Cuda );

			dim3 threads = dim3( 32, 6, 1 );
			dim3 blocks = dim3( (numClusters + 6 - 1) / 6, 1, 1 );

			unsigned nextMult = (numClusters / 1024 + 1) * 1024;

			// initialize buffers
			m_clusterLightCounts.resize( nextMult, true );
			m_clusterLightDataOffsets.resize( nextMult, true );


			// initializer cluster aabber
			unsigned grid2dDim = (m_globals.fbHeight + LIGHT_GRID_TILE_DIM_Y - 1) / LIGHT_GRID_TILE_DIM_Y;

			float sD = 2.0f * tanf( toRadians(m_globals.fov) * 0.5f ) / grid2dDim;
			ClusterAabbBuilder clusterAabbBuilder(m_globals, sD);

			// count light overlaps
			{
				PROFILE_SCOPE_2( "assign_count", TT_Cuda );
				light_hierarchy_assign_count<<<blocks,threads>>>( lightDataTexture, *m_lightHierarchy, clusterAabbBuilder, m_uniqueClusterIds.cudaPtr(), numClusters, m_clusterLightCounts.cudaPtr() );
				CHECK_CUDA_ERROR();
			}

			light_hierarchy_assign_scan<<<1,1024>>>( m_clusterLightCounts.cudaPtr(), numClusters, nextMult, m_clusterLightDataOffsets.cudaPtr() );
			CHECK_CUDA_ERROR();

			// count light overlaps
			{
				PROFILE_SCOPE_2( "assign_store", TT_Cuda );
				light_hierarchy_assign_store<<<blocks,threads>>>( lightDataTexture, *m_lightHierarchy, clusterAabbBuilder, m_uniqueClusterIds.cudaPtr(), m_clusterLightDataOffsets.cudaPtr(), numClusters, m_tileLightIndexListBuffer->getPointer() );
				CHECK_CUDA_ERROR();
			}


#if ENABLE_LIGHT_COUNTER
			{
				static uint32_t* hostCountBuffer = 0;
				static size_t hostCountBufferSize = 0;

				if( hostCountBufferSize < numClusters )
				{
					hostCountBufferSize = numClusters;

					delete [] hostCountBuffer;
					hostCountBuffer = new uint32_t[hostCountBufferSize];
				}

				cudaMemcpy( hostCountBuffer, m_clusterLightCounts.cudaPtr(), sizeof(uint32_t)*numClusters, cudaMemcpyDeviceToHost );

				uint32_t totalLights = 0;
				for( size_t i = 0; i < numClusters; ++i )
					totalLights += hostCountBuffer[i];

				uint32_t rejectedLights = 0;
				cudaMemcpyFromSymbol( &rejectedLights, g_totalLightsRejected, sizeof(uint32_t), 0, cudaMemcpyDeviceToHost );

				uint32_t zero = 0;
				cudaMemcpyToSymbol( g_totalLightsRejected, &zero, sizeof(uint32_t), 0, cudaMemcpyHostToDevice );

				MY_PROFILE_COUNTER( "TotalLightsInClusters", totalLights );
				MY_PROFILE_COUNTER( "TotalRejectedLights", rejectedLights );
			}
#endif // ~ ENABLE_LIGHT_COUNTER

		}
	}
}


__global__ void storeToGrid(uint32_t numClusters, const uint32_t *__restrict__ uniqueClustersIndexes, const uint32_t *__restrict__ clusterLightCounts, const uint32_t *__restrict__ clusterLightOffsets, uint2 *__restrict__ resultGridBuffer)
{
	uint32_t index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index < numClusters)
	{
		uint32_t clusterIndex = uniqueClustersIndexes[index];
		resultGridBuffer[clusterIndex] = make_uint2(clusterLightOffsets[index], clusterLightCounts[index]);
	}
}



void CudaRendererImpl::buildClusters(const Globals &globals, uint32_t numLights)
{
	PROFILE_SCOPE_2("buildClusters", TT_Cuda);

	m_globals = globals;
	CUDA_UPLOAD_GLOBALS(m_globals);

	uint32_t grid2dDimY = (globals.fbHeight + LIGHT_GRID_TILE_DIM_Y - 1) / LIGHT_GRID_TILE_DIM_Y;
	float sD = 2.0f * tanf(toRadians(globals.fov) * 0.5f) / float(grid2dDimY);
	float zGridLocFar = logf(globals.far / globals.near)/logf(1.0f + sD);

	const uint3 gridDim = make_uint3((globals.fbWidth + LIGHT_GRID_TILE_DIM_X - 1) / LIGHT_GRID_TILE_DIM_X, grid2dDimY, uint32_t(ceilf(zGridLocFar) + 0.5f));

	PROFILE_COUNTER("GridDimX", gridDim.x);
	PROFILE_COUNTER("GridDimY", gridDim.y);
	PROFILE_COUNTER("GridDimZ", gridDim.z);

	chag::CudaBaseResource *resouces[] = 
	{
		m_clusterFlagBuffer,
		m_clusterResultBuffer,
		m_lightPositionRangeBuffer,
		m_tileLightIndexListBuffer,
	};

	{
		PROFILE_BEGIN_BLOCK_2("map", TT_Cuda);
		// map resources in this block, auto unmaps at end.
		chag::CudaResourceBatchMapper mappo(resouces, sizeof(resouces) / sizeof(resouces[0]));
		PROFILE_END_BLOCK_2();

		cudaMemcpyToSymbol(g_numLights, &numLights, sizeof(numLights), 0, cudaMemcpyHostToDevice);

		const uint32_t numGridCells = LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y * LIGHT_GRID_MAX_DIM_Z;
		uint32_t numClusters = 0;
		// 0. Find unique clusters (compaction)
		uint32_t *clusterFlags = m_clusterFlagBuffer->getPointer();
		{
			PROFILE_SCOPE_2("chag::pp::prefix-clusterFlags", TT_Cuda);
		chag::pp::prefix(clusterFlags, clusterFlags + numGridCells, m_tmpBuffer.cudaPtr(), m_numClustersCuda.cudaPtr());
		}
		m_numClustersCuda.copyToHost(&numClusters, 1, true);

		{
			PROFILE_SCOPE_2("clearResultBuffer", TT_Cuda);
		// clearing the result buffer is not strictly necessary, as no cells which are not used ought to be accessed.
		megaMemset(reinterpret_cast<uint32_t*>(m_clusterResultBuffer->getPointer()), 0, numGridCells * 2);
		}
		// adjust storage for clusters
		m_numClustersCuda.sync();
		PROFILE_COUNTER("NumClusters", numClusters);
		m_uniqueClusterIds.resize(numClusters, true);
		m_uniqueClusterIndexes.resize(numClusters, true);

		// If no geometry on screen, no clusters and nothing more to do...
		if (numClusters)
		{
			{
				PROFILE_SCOPE_2("storeUniqueClusters", TT_Cuda);
			// 1. Store actual clusters.
			storeUniqueClusters<<<getBlockCount(192, numGridCells), 192>>>(numGridCells, clusterFlags, m_tmpBuffer.cudaPtr(), m_uniqueClusterIds.cudaPtr(), m_uniqueClusterIndexes.cudaPtr());
			}
			// 2. Assign lights.
			assignLights(numLights, numClusters, m_lightPositionRangeBuffer->getPointer());

			{
				PROFILE_SCOPE_2("storeToGrid", TT_Cuda);
			// 3. Store back to full 3d light grid
			storeToGrid<<<getBlockCount(192, numClusters), 192>>>(numClusters, m_uniqueClusterIndexes.cudaPtr(), m_clusterLightCounts.cudaPtr(), m_clusterLightDataOffsets.cudaPtr(), reinterpret_cast< ::uint2 *>(m_clusterResultBuffer->getPointer()));
			}
		}

		{
			PROFILE_SCOPE_2("clearFlags", TT_Cuda);
		// 4. clear the grid buffer, for next round...
		megaMemset(clusterFlags, 0, numGridCells);
		}
	}
}
