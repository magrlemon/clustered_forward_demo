Demo code implementing Clustered Forward Shading, accompanying the talk 
'Tiled and Clustered Forward Shading' presented at Siggraph 2012.

For other projects, papers and source code, or if you wish to contact the author go to:
http://efficientshading.com/

Update: now with visual studio 2015 support.

Running the demo:
------------------
Start run.cmd.




BibTex entry:
---------------
@inproceedings{OlssonBilleterAssarsson2012,
 author = {Olsson, Ola and Billeter, Markus and Assarsson, Ulf},
 title = {Tiled and Clustered Forward Shading},
 booktitle = {SIGGRAPH '12: ACM SIGGRAPH 2012 Talks},
 year = {2012},
 location = {Los Angeles, California},
 publisher = {ACM},
 address = {New York, NY, USA},
 abstract = {Tiled and Clustered Forward Shading are new techniques that enable support for thousands of lights, while eliminating many of the drawbacks of deferred techniques. Ths talk shows how these techniques can be used and extended to support transparency with high efficiency.},
}



Overview
-----------
The quick takeaway is that the demo implements finding the required clusters 
using an extra render pass, drawing all transparent geometry. This then enables
light assignment to work out what lights are needed for all geometry. Another
major difference from our paper about Clustered Deferred Shading, is that we 
here do not use a virtualized grid, but a concrete, full grid data structure. 
This is much simpler to implement and also faster, and hence, a good choice if 
resolution and tile size can be managed to keep memory requirements in line.



Release Notes:
----------------
* The tile size is set to 64x64 pixles by default, which keeps the number of 
  cells managable for CPU side full grid light assignment (used when OpenGL 4 
  features are available). Incidentally, this might not be a bad idea anyway, 
  as clusters are subdivided along z making them on average smaller.

* The code used to downsample the depth buffer is awful (but simple...), so 
  anyone wanting to make use of this technique for real need to replace it 
  with something vaguely clever, ideally a compute shader pass.

* Beware: The SimpleShader class, used to manage glsl shaders, does not properly
  pre-process the files. Instead includes simply substitute the include
  directive, wherever it might occur. E.g. inside comments...



Known Issues:
---------------  
* Clustered Forward does reportedly not work correctly after the first frame on
  an AMD Radeon HD 7970. The problem has not been fully dignosed and I do not 
  have access to any OpenGL 4 capable AMD card. Possibly the full CPU fallback
  works, as it works on my AMD Radeon HD 3100 integrated. To force this
  set g_enableGpuClustering to false at the declaration. 



Usage Instructions
---------------------
Run the executable by starting the file 'run.cmd'. If this fails, the most
probable reason is that the system doesn't have the Visual Studio 
redistributable package(s) installed. Run 'install_redist.cmd' to install
the redistributables for both Visual C++ 2008 and 2005, this is required as
the DevIL binaries are linked against the 2005 version.

By default the demo will load the scene 'data/crysponza_bubbles/sponza.obj', 
which is a modified and repackaged version of the scene made available by 
Crytek:
http://www.crytek.com/cryengine/cryengine3/downloads

To use another scene, just replace the command line argument. The only 
supported scene format is obj.

When the demo is running, pressing <F1> brings up an info/help text which 
provides details on other function keys. The help text also provides some
stats, such as number of lights used. For futher stats and performance measures
press <F2>, which shows a profile tree, with counters and timers. All timings 
are in milliseconds.

To change the maximum number of lights supported and other compile time 
options, see Config.h.



Programmer Guide
------------------
The most interesting files ought to be:

'clustered_forward_demo.cpp' - contains the main program logic, and is admittedly
a bit of a monster. The rendering is controlled from the function onGlutDisplay,
so why not start there? The enum called RenderMethod is used to select which 
type of shading is performed so that should help guide your reading.

'shaders/clusteredShading.glsl' - in which all the logic and uniforms needed to 
compute shading for a sample in a cluster, is contained. This file is included from
'clustered_forward_fragment.glsl', and used to compute shading. 

'LightGrid.h/cpp' - contains the logic needed to construct the light grid on the
CPU.

'Config.h' - as noted before, this is where some of the core program behaviour
can be configured. For example, maximum number of lights and grid resolution.
Note that these properties may be subject to hardware/API restrictions, read
associated comments carefully.

'CudaRenderer.cu' - is the place where the CUDA version of the Clustered Forward 
Shading implementation lives. It uses the hierarchical light assignment of our 
paper, as well as efficient CUDA primitives to find the compact list of used
clusters.



Buiding the source
--------------------
This should be as easy as opening the solution and pressing whatever button 
does the build for you. Assuming, of course, that you have Visual Studio 2008
installed. There are are binaries and libraries for 32-bit and 64-bit targets. 

There are sevaral project configurations, those that use CUDA and those that
dont, e.g. Release_CUDA vs Release. If you do not plan to run CUDA code, you 
can save a fair bit of compile time using the non-cuda configurations.

No other OS or environment is directly supported. However, most (all?) of the 
source exist with a linux port, so a conversion should be relatively straight-
forward. The libraries that the demo depends on must be acquired independently.
These are: DevIL, freeglut, and glew (and CUDA, if building those). 



System Requirements:
-----------------------
Windows XP or above
Graphics Card + Driver supporting OpenGL 3.3 (4.1 for all features (NVIDIA for CUDA...))




References:
------------
[1]	Tiled Shading, Ola Olsson and Ulf Assarsson
[2] Clustered Deferred and Forward Shading, Ola Olsson, Markus Billeter and Ulf Assarsson
[3]	Tiled and Clustered Forward Shading, Ola Olsson, Markus Billeter and Ulf Assarsson
	http://efficientshading.com/19-2/

[4]	My blog post about clustered shading attempts to list a number of sources.
	http://efficientshading.com/2016/09/18/clustered-shading-in-the-wild/
	
[5] Otherwise, use google I guess... :)
    https://www.google.com/search?q=clustered+shading


Thanks:
----------
For testing and feedback
* Damian Trebilco
* Emil Persson
* Matias N. Goldberg
