/*********************************************************** -- HEAD -{{{1- */
/** Light Assignment Implementation
 */
/******************************************************************* -}}}1- */

/* Warning. Make sure that no <linmath/foo> stuff is included here. Even 
 * indirectly. There are some issues between the chag::typeN types and 
 * chag::pp. For some reason the compiler ends up trying to use chag::typeN
 * in a later compilation step. (Bad.)
 */

#include <float.h>
#include <chag/pp/reduce.cuh>

//--//////////////////////////////////////////////////////////////////////////
//--    $ helpers                    ///{{{1//////////////////////////////////
namespace
{
	struct SphereAABBMinOp
	{
		__device__ __host__ ::float4 operator() (::float4 aA, ::float4 aB) const
		{
			return make_float4( 
				min(aA.x-aA.w,aB.x-aB.w), 
				min(aA.y-aA.w,aB.y-aB.w), 
				min(aA.z-aA.w,aB.z-aB.w), 
				0.0f
			);
		}
		__device__ __host__ ::float4 identity() const
		{
			return make_float4( +FLT_MAX, +FLT_MAX, +FLT_MAX, 0.0f );
		}
	};
	struct SphereAABBMaxOp
	{
		__device__ __host__ ::float4 operator() (::float4 aA, ::float4 aB) const
		{
			return make_float4( 
				max(aA.x+aA.w,aB.x+aB.w), 
				max(aA.y+aA.w,aB.y+aB.w), 
				max(aA.z+aA.w,aB.z+aB.w), 
				0.0f
			);
		}
		__device__ __host__ ::float4 identity() const
		{
			return make_float4( -FLT_MAX, -FLT_MAX, -FLT_MAX, 0.0f );
		}
	};
}

//--    LH : compute_light_aabb()    ///{{{1//////////////////////////////////
void light_hierarchy_compute_light_aabb( const ::float4* aLightsStart, const ::float4* aLightsEnd, ::float4* aLightAABBMin, ::float4* aLightAABBMax )
{
	chag::pp::reduce( aLightsStart, aLightsEnd, aLightAABBMin, SphereAABBMinOp() );
	chag::pp::reduce( aLightsStart, aLightsEnd, aLightAABBMax, SphereAABBMaxOp() );
}

//--///}}}1////////////// vim:syntax=cuda:foldmethod=marker:ts=4:noexpandtab: 
