#ifndef _PrimitiveRenderer_h_
#define _PrimitiveRenderer_h_

#include <linmath/float3.h>
#include <linmath/float2.h>
#include <linmath/float4x4.h>
#include <linmath/aabb.h>
#include <GL/glew.h>
#include <utils/GlBufferObject.h>

namespace chag
{

class SimpleShader;

class PrimitiveRenderer
{
public:
	void init();

	void drawSphere(const float3 &pos, float size, SimpleShader *shader, bool wireFrame = false);
	void drawAabb(const chag::Aabb &aabb, SimpleShader *shader, bool wireFrame = false);
	void drawPlane(chag::float3 normal, chag::float3 point, chag::float3 alignToDir, float planeSize, SimpleShader *shader);
	void drawPlane(chag::float3 normal, chag::float3 point, float planeSize, SimpleShader *shader);
	void drawQuadGrid(chag::float3 normal, chag::float3 point, chag::float3 alignToDir, float planeSize, SimpleShader *shader);
	void drawQuadGrid();
	void drawCone(const float3 &pos, const float3 &dir, float angle, float length, SimpleShader *shader, bool wireFrame = false);
	enum AttributeArrays
  {
    AA_Position = 0,
    AA_Normal,
    AA_TexCoord,
    AA_Max,
  };

	/**
	 * Bit of a hack (and not very efficient either...).
	 */
	void setTransform(const chag::float4x4 &tfm) { m_transform = tfm; }

private:

	void createSphereModel();
	void buildAabb();
	void buildPlane();
	void buildQuadGrid();
	void createConeMesh();

	GlBufferObject<chag::float3> g_aabbVerts;
	GlBufferObject<chag::float3> g_aabbNormals;
	GlBufferObject<chag::float2> g_aabbUvs;
	GlBufferObject<int> g_aabbInds;
	GLuint g_aabbVaob;

	GLuint m_sphereVaob;
	GlBufferObject<chag::float3> g_sphereVertexBuffer;

	GlBufferObject<chag::float3> g_planeVerts;
	GlBufferObject<chag::float3> g_planeNormals;
	GlBufferObject<chag::float2> g_planeUvs;
	GLuint g_planeVaob;


	GlBufferObject<chag::float3> g_quadGridVerts;
	GlBufferObject<chag::float3> g_quadGridNormals;
	GlBufferObject<chag::float2> g_quadGridUvs;
	GlBufferObject<int> g_quadGridInds;
	GLuint g_quadGridVaob;


	GlBufferObject<chag::float3> g_coneVerts;
	GlBufferObject<chag::float3> g_coneNormals;
	GlBufferObject<chag::float2> g_coneUvs;
	GlBufferObject<int> g_coneInds;
	GLuint g_coneVaob;

	chag::float4x4 m_transform;

	const int s_maxQuadGrid = 16;
};


}; // namespace chag


#endif // _PrimitiveRenderer_h_
